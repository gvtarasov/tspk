// --------------------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// 5725-A06 5725-A29 5724-Y48 5724-Y49 5724-Y54 5724-Y55
// Copyright IBM Corporation 1998, 2013. All Rights Reserved.
//
// Note to U.S. Government Users Restricted Rights:
// Use, duplication or disclosure restricted by GSA ADP Schedule
// Contract with IBM Corp.
// --------------------------------------------------------------------------

// ����� ����� � ����������� ��������������� + 2 ��������� (= ��������� ������ �� ����� 
// ����������� ������������, ������ ��� �� ��������� ���� � ��� �� �������). ������� ������� - makespan

using CP;

{string} WorkerNames = ...;
{string} TaskNames = ...;

int    Duration [t in TaskNames] = ...;
string Worker   [t in TaskNames] = ...;

tuple Precedence {
   string pre;
   string post;
};

{Precedence} Precedences = ...;

dvar interval Processing [t in TaskNames] size Duration[t];

dvar sequence workers[w in WorkerNames] in
    all(t in TaskNames: Worker[t]==w) Processing[t];// types
    //all(t in TaskNames: Worker[t]==w) ;
  

minimize max(t in TaskNames) 
  endOf(Processing[t]);

subject to {
  forall(p in Precedences)
      endBeforeStart(Processing[p.pre], Processing[p.post]);
        
  forall(w in WorkerNames)
    noOverlap(workers[w]);      
        
 // forall(w in WorkerNames)
 //   noOverlap( all(t in TaskNames: Worker[t]==w) Processing[t]);      
}

execute {
cp.param.FailLimit = 20000;
}
