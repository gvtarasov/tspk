--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

--
-- Data for Name: hwtable; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO hwtable (id, name, age, address, salary) VALUES (1, 'Alice', 21, 'Ohio                                              ', 2500);
INSERT INTO hwtable (id, name, age, address, salary) VALUES (7, 'Bob', 25, 'Michigan                                          ', 3000);
INSERT INTO hwtable (id, name, age, address, salary) VALUES (2, 'Caroline', 22, 'Salt Lake City                                    ', 3500);
INSERT INTO hwtable (id, name, age, address, salary) VALUES (4, 'Doug', 29, 'Chicago                                           ', 10000);
INSERT INTO hwtable (id, name, age, address, salary) VALUES (3, 'Earl', 23, 'Detroit                                           ', 2000);


--
-- Data for Name: hwtable2; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: hwtable3; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- PostgreSQL database dump complete
--

