--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: hwtable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hwtable (
    id integer NOT NULL,
    name text NOT NULL,
    age integer NOT NULL,
    address character(50),
    salary real
);


ALTER TABLE hwtable OWNER TO postgres;

--
-- Name: hwtable2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hwtable2 (
    id integer NOT NULL,
    dept character(50) NOT NULL,
    emp_id integer NOT NULL
);


ALTER TABLE hwtable2 OWNER TO postgres;

--
-- Name: hwtable3; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE hwtable3 (
    field1 integer NOT NULL,
    field2 text NOT NULL
);


ALTER TABLE hwtable3 OWNER TO postgres;

--
-- Data for Name: hwtable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hwtable (id, name, age, address, salary) FROM stdin;
1	Alice	21	Ohio                                              	2500
7	Bob	25	Michigan                                          	3000
2	Caroline	22	Salt Lake City                                    	3500
4	Doug	29	Chicago                                           	10000
3	Earl	23	Detroit                                           	2000
\.


--
-- Data for Name: hwtable2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hwtable2 (id, dept, emp_id) FROM stdin;
\.


--
-- Data for Name: hwtable3; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hwtable3 (field1, field2) FROM stdin;
\.


--
-- Name: hwtable2 hwtable2_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hwtable2
    ADD CONSTRAINT hwtable2_pkey PRIMARY KEY (id);


--
-- Name: hwtable3 hwtable3_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hwtable3
    ADD CONSTRAINT hwtable3_pkey PRIMARY KEY (field1);


--
-- Name: hwtable hwtable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY hwtable
    ADD CONSTRAINT hwtable_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

